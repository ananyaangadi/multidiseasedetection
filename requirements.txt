tqdm==4.31.1
tensorflow==1.13.1
scikit-learn==0.20.3
Pillow==5.1.0
pandas==0.23.4
numpy==1.16.3
matplotlib==3.0.3
efficientnet