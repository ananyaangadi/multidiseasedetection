This reproducible research repository accompanies the paper,  
A. Angadi, A. N. Bhat, P. Ankitha, P. S. Kumar and G. Srinivasa, **"Deep learning based pipeline for multi-disease detection"**,  
4th International Conference on Machine Intelligence and Signal Processing (MISP2022). Submitted.  
Please cite this paper if any code or model in this repository is used in your work. 

## Data

The complete data can be found at the challenge website: https://riadd.grand-challenge.org/  
Training Dataset (1920 images): https://drive.google.com/file/d/1Eiu8PH4A50rkmDzYxO6ZTHNtFEbTNkl_/view  
Evaluation Set (640 images): https://drive.google.com/file/d/1gndZ94mzTHUnS_0B7zcQFSCPjMWfyUU_/view  
<br/>
The preprocessed data we have used in our models can be found at:  
Training: https://www.kaggle.com/ananyaangadi/cropped-rfmid  
Testing: https://www.kaggle.com/ananyaangadi/testdataset

## Repository Directory Structure

.  
├── Models  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── DR_MODEL.h5 - Trained model for Diabetic Retinopathy  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── MH_MODEL.h5 - Trained model for Media Haze  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── ODE_MODEL.h5 - Trained model for Optic Disc Edema  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── MYA_MODEL.h5 - Trained model for Myopia  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── ARMD_MODEL.h5 - Trained model for Age-Related Macular Degeneration  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── RS_MODEL.h5 - Trained model for Retinitis  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; └── DiseaseRisk_MODEL.h5 - Trained model for Disease Risk   
├── Notebooks  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── EDA.ipynb - Exploratory Data Analysis  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── Training  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; │ &nbsp; &nbsp; ├── VanillaCNN_DR.ipynb - Code to train DR model  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; │ &nbsp; &nbsp; ├── VanillaCNN_MH.ipynb - Code to train MH model  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; │ &nbsp; &nbsp; ├── EfficientNetDN(Rep).ipynb - Code to train DN model  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; │ &nbsp; &nbsp; ├── EfficientNetMYA(Rep).ipynb - Code to train MYA model  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; │ &nbsp; &nbsp; ├── EfficientNetARMD(Rep).xpynb - Code to train AMRD model  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; │ &nbsp; &nbsp; ├── EfficientNetODE(Rep).ipynb - Code to train ODE model  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; │ &nbsp; &nbsp; ├── EfficientNetRS(Rep).xpynb - Code to train RS model  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; │ &nbsp; &nbsp; └── EfficientNetDiseaseRisk.ipynb - Code to train Disease Risk model  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; └── Testing  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── TestDR.ipynb - Run DR model on test data  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── TestMH.ipynb - Run MH model on test data  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── TestDN.ipynb - Run DN model on test data  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── TestMYA.ipynb - Run MYA model on test data  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── TestARMD.ipynb - Run ARMD model on test data  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── TestODE.ipynb - Run ODE model on test data  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├── TestRS.ipynb - Run RS model on test data  
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; └── TestDiseaseRisk.xpynb - Run Disease Risk model on test data  
├── README.md  
└── requirements.txt  


## Setup
pip install -r requirements.txt